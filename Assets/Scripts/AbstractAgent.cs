﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AbstractAgent<Ci, Ce> : MonoBehaviour  	where Ci : struct
																where Ce : struct
{
	protected AbstractBehavior<Ci>[] behaviors;
	protected AbstractSensor[] sensors;
	protected AbstractActuator<Ce>[] actuators; 

	protected Ci internalContext;
	public Ce ExternalContext;

	protected A CallActuator<A>(string id, bool actCondition, float intensity) where A : AbstractActuator<Ce>
	{
		var act = GetActuator<A>(id);
		if(act != null) {
			if(actCondition) act.Act(intensity, ref ExternalContext);
			else if(act.IsActive) act.Deact();
		}
		return act;
	}

	protected A GetActuator<A>(string id) where A : AbstractActuator<Ce>
	{
		int len = actuators.Length;
		for (int i = 0; i < len; i++) 
		{
			var act = actuators[i];
			if(act != null && act.Id == id)
			{
				return act as A;
			}
		}
		
		return null;
	}
	
	protected A GetActuator<A>(System.Type clazz) where A : AbstractActuator<Ce>
	{
		int len = actuators.Length;
		for (int i = 0; i < len; i++) 
		{
			var act = actuators[i];
			if(act != null && act.GetType() == clazz)
			{
				return act as A;
			}
		}
		return null;
	}
	
	protected A[] GetActuators<A>(System.Type clazz) where A : AbstractActuator<Ce>
	{
		int len = actuators.Length;
		List<A> ret = new List<A> ();
		
		for (int i = 0; i < len; i++) 
		{
			var act = actuators[i];
			if(act != null && act.GetType() == clazz)
			{
				ret.Add(act as A);
			}
		}
		
		return ret.Count > 0 ? ret.ToArray () : null;
	}

	virtual protected void Start () 
	{
		behaviors = GetComponents<AbstractBehavior<Ci>> ();
		actuators = GetComponentsInChildren<AbstractActuator<Ce>> ();
		sensors = GetComponentsInChildren<AbstractSensor> ();

		foreach (var behavior in behaviors) {
			behavior.Initialize(sensors);
		}
		foreach (var actuator in actuators) {
			actuator.Initialize(this);
		}
	}
	
	virtual protected void FixedUpdate () 
	{		
		foreach (var behavior in behaviors) 
		{
			if (!behavior.IsSuppressedBy(behaviors)) 
			{
				behavior.Execute(ref internalContext);
			}
		}
		//Debug.Log("--------------------");
	}
}
