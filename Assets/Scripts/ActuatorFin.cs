﻿using UnityEngine;
using System.Collections;

public class ActuatorFin : AbstractActuator<AquariumAgentContext> 
{
	const float MAX_TURN_DEGREE = 360f;
	Vector3 MAX_ANIM_DEGREE = new Vector3(0,35);

	int side = 0;

	public enum FinType
	{
		Back = 1,
		Left = 2,
		Right = 3
	}

	public FinType Type;

	#region implemented abstract members of AbstractActuator

	public override void Act (float intensity, ref AquariumAgentContext externalContext)
	{
		intensity = Mathf.Abs(intensity);
		var degree = intensity * MAX_ANIM_DEGREE;
		//Debug.Log("Actuator [" + Type + "] act = " + intensity);
		switch (Type) {
		case FinType.Back:
			side = side <= 0 ? 1 : -1;
			transform.localEulerAngles = side * Mathf.Max(0.5f, intensity) * degree;
			externalContext.Speed = intensity;
			break;
		case FinType.Left:
			transform.localEulerAngles = intensity * degree;
			externalContext.LocalDirection.y += MAX_TURN_DEGREE/360 * -intensity;
			break;
		case FinType.Right:
			transform.localEulerAngles = -intensity * degree;
			externalContext.LocalDirection.y += MAX_TURN_DEGREE/360 * intensity;
			break;
		}
		IsActive = true;
	}
	
	public override void Deact ()
	{
		transform.localEulerAngles = Vector3.zero;
		IsActive = false;
	}

	#endregion


	void Update()
	{

	}
}
