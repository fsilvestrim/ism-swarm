﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

abstract public class AbstractBehavior<C> : SubssumptionBehavior where C : struct 
{
	private AbstractSensor[] sensors;
	private System.Type type;
	protected bool IsActive;

	public void Initialize(AbstractSensor[] sensors)
	{
		this.type = this.GetType();
		this.sensors = sensors;

		int len = suppresses.Length;
		suppressesType = new System.Type[len];

		for (int i = 0; i < len; i++)
		{
		      	var rf = suppressesTypeStr[i];
				if (rf == null) 
					suppressesType[i] = null;
		      	else 
				{
					suppressesType[i] = System.Type.GetType(rf);
		      	}
		}
	}

	public bool IsSuppressedBy (AbstractBehavior<C>[] bhvs)
	{
		int len = bhvs.Length;
		
		for (int i = 0; i < len; i++) 
		{
			var bhv = bhvs[i];
			if(bhv != this && bhv.Suppresses(this))
			{
				return true;
			}
		}

		return false;
	}

	public bool Suppresses (AbstractBehavior<C> bhv)
	{
		if(bhv == this || !IsActive) return false;
		int len = suppressesType.Length;

		for (int i = 0; i < len; i++) 
		{
			var tp = suppressesType[i];
			//Debug.Log(tp + " ?????? " + bhv.type);
			if (tp == bhv.type)
			{
				return true;
			}
		}

		return false;
	}

	protected S GetSensor<S>(string id) where S : AbstractSensor
	{
		int len = sensors.Length;
		for (int i = 0; i < len; i++) 
		{
			var sensor = sensors[i];
			if(sensor != null && sensor.Id == id)
			{
				return sensor as S;
			}
		}

		return null;
	}

	protected S GetSensor<S>(System.Type clazz) where S : AbstractSensor
	{
		int len = sensors.Length;
		for (int i = 0; i < len; i++) 
		{
			AbstractSensor sensor = sensors[i];
			if(sensor != null && sensor.GetType() == clazz)
			{
				return sensor as S;
			}
		}
		return null;
	}

	protected S[] GetSensors<S>(System.Type clazz) where S : AbstractSensor
	{
		int len = sensors.Length;
		List<S> ret = new List<S> ();

		for (int i = 0; i < len; i++) 
		{
			var sensor = sensors[i];
			if(sensor != null && sensor.GetType() == clazz)
			{
				ret.Add(sensor as S);
			}
		}

		return ret.Count > 0 ? ret.ToArray () : null;
	}
	
	abstract public void Execute(ref C context);
}

[System.Serializable]
public class SubssumptionBehavior : MonoBehaviour 
{
	[SerializeField, HideInInspector]
	protected Object[] suppresses;

	[SerializeField, HideInInspector]
	protected string[] suppressesTypeStr;

	protected System.Type[] suppressesType;
}