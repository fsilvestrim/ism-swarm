﻿using UnityEngine;
using System.Collections;

public class SensorVision : AbstractSensor {

	public bool Collides {
		private set;
		get;
	}

	void OnTriggerEnter(Collider other)
	{
		Collides = true;
	}

	void OnTriggerExit(Collider  other) 
	{
		Collides = false;
	}
}
