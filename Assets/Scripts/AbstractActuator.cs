﻿using UnityEngine;
using System.Collections;

public abstract class AbstractActuator<Ce> : MonoBehaviour 	where Ce : struct
{
	public string Id;
	
	public bool IsActive
	{
		protected set;
		get;
	}

	protected MonoBehaviour agent
	{
		private set;
		get;
	}

	public virtual void Initialize (MonoBehaviour agent)
	{
		this.agent = agent;
	}

	public abstract void Act (float intensity, ref Ce externalContext);
	public abstract void Deact ();
}
