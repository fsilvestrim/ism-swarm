﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(SubssumptionBehavior), true)]
public class SubssumptionBehaviorEditor : Editor 
{
	private ReorderableList resourcesList;
	
	public override void OnInspectorGUI() 
	{
		serializedObject.Update();
		
		if (resourcesList == null && serializedObject.FindProperty("suppresses") != null)
		{
			string lastString = null;
			var typesList = serializedObject.FindProperty("suppressesTypeStr");
			resourcesList = new ReorderableList(serializedObject, serializedObject.FindProperty("suppresses"), true, true, true, true);
			resourcesList.drawHeaderCallback = (Rect rect) => { EditorGUI.LabelField(rect, "suppresses "); };
			resourcesList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => 
			{
				typesList.arraySize = Mathf.Max(typesList.arraySize, index + 1);

				var element = resourcesList.serializedProperty.GetArrayElementAtIndex(index);
				var typeProp = typesList.GetArrayElementAtIndex(index);

				EditorGUI.PropertyField(
					new Rect(rect.x, rect.y, 250, EditorGUIUtility.singleLineHeight),
					element, GUIContent.none);

				if (typeProp != null && typeProp.stringValue != lastString)
				{
					typeProp.stringValue = (element.objectReferenceValue as MonoScript).GetClass().FullName;
				}
				
				lastString = typeProp.stringValue;
			};
		}
		
		EditorGUILayout.Separator();
		resourcesList.DoLayoutList();
		EditorGUILayout.Separator();
		serializedObject.ApplyModifiedProperties();
	}
}
