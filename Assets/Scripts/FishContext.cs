using UnityEngine;
using System.Collections;

public struct FishContext 
{
	public Transform ConstTransform;
	public Vector3 ConstWorldForward;
	public Vector3 ConstWorldPosition;
	public Vector3 LocalAngularDirection;
	public float Speed;
	public float Hunger;
	public float Safety;
	public float Tired;
}