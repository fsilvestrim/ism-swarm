﻿using UnityEngine;
using System.Collections;

public class SensorSonar : AbstractSensor {
	
	public bool Collides {
		private set;
		get;
	}

	public Vector3 Size {
		private set;
		get;
	}

	void Start ()
	{
		var collider = GetComponent<BoxCollider>();
		Size = collider.size;
	}
	
	void OnTriggerEnter(Collider other)
	{
		Collides = true;
	}
	
	void OnTriggerExit(Collider  other) 
	{
		Collides = false;
	}
}
