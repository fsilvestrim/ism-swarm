using UnityEngine;
using System.Collections;

[System.Serializable]
public class BehaviorWandering : AbstractBehavior<FishContext>
{

	private const double WANDERING_PROBABILITY = 0.02d;
	private const float DISTANCE_EPYSLON = 1f;

	private readonly System.Random random = new System.Random();

	private Vector3 WorldTargetPosition;

	#region implemented abstract members of AbstractBehavior

	public override void Execute (ref FishContext internalContext)
	{
		if((WorldTargetPosition.magnitude == 0 && random.NextDouble() < WANDERING_PROBABILITY) || (internalContext.ConstWorldPosition - WorldTargetPosition).sqrMagnitude <= DISTANCE_EPYSLON * DISTANCE_EPYSLON)
		{
			var sonar = GetSensor<SensorSonar>("sonar");
			if(sonar != null)
			{
				WorldTargetPosition = (-sonar.Size * .5f) + (sonar.Size * (float)random.NextDouble());
			}
			else 
			{
				WorldTargetPosition = new Vector3(5,5,5) + (new Vector3(10,10,10) * (float)random.NextDouble());
			}

			WorldTargetPosition.y = 0;
			internalContext.Speed = (float)random.NextDouble();
		}

		internalContext.LocalAngularDirection += internalContext.ConstTransform.InverseTransformVector(WorldTargetPosition - internalContext.ConstWorldPosition).normalized;

		//Debug.Log(this);
		IsActive = true;
	}

	#endregion

	void OnDrawGizmos ()
	{
		Gizmos.color = Color.white;
		Gizmos.DrawWireSphere(WorldTargetPosition, DISTANCE_EPYSLON*.5f);
	}
}
