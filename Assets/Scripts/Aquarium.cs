﻿using UnityEngine;
using System.Collections;

public class Aquarium : MonoBehaviour 
{
	private const float FRICTION = .8f;
	private FishAgent[] Fishes;
	
	void Start () 
	{
		Fishes = FindObjectsOfType<FishAgent>();
	}

	void FixedUpdate () 
	{
		foreach (var fish in Fishes) 
		{
			var WorldDirection = fish.transform.TransformDirection(fish.ExternalContext.LocalDirection);
			//if(Mathf.Abs(fish.ExternalContext.LocalDirection.z) > 0.05f)
			//	fish.transform.rotation = Quaternion.Slerp(fish.transform.rotation, Quaternion.LookRotation (WorldDirection, Vector3.up), Time.deltaTime);

			//fish.transform.rotation = Quaternion.Slerp(fish.transform.rotation, Quaternion.LookRotation (WorldDirection, Vector3.up), Time.deltaTime * fish.ExternalContext.Speed);
			fish.transform.Rotate(fish.ExternalContext.LocalDirection, Space.Self);
			if(fish.ExternalContext.Speed > 0)
			{
				fish.transform.Translate(-Vector3.right * fish.ExternalContext.Speed * Time.deltaTime, Space.Self);
				fish.ExternalContext.Speed *= FRICTION;
			}
			fish.ExternalContext.LocalDirection = Vector3.zero;
		}
	}
}

public struct AquariumAgentContext 
{
	public float Speed;
	public Vector3 LocalDirection;
}
