﻿using UnityEngine;
using System.Collections;

public class BehaviorAvoidance : AbstractBehavior<FishContext> 
{
	private const float SPEED = 0.8f;
	private const string LEFT_EYE = "left_eye";
	private const string RIGHT_EYE = "right_eye";

	private Vector3 normals;

	#region implemented abstract members of AbstractBehavior

	public override void Execute (ref FishContext internalContext)
	{
		normals = Vector3.zero;
		IsActive = false;

		var left = GetSensor<SensorVision>(LEFT_EYE);
		var right = GetSensor<SensorVision>(RIGHT_EYE);

		var collidesLeft = left != null && left.Collides;
		var collidesRight = right != null && right.Collides;

		RaycastHit hit = new RaycastHit();

		if((collidesLeft || collidesRight) && Physics.Raycast(internalContext.ConstWorldPosition, internalContext.ConstWorldForward, out hit, 1f)) 
		{
			IsActive = true;
			normals += hit.normal;
		}

		if(collidesLeft && Physics.Raycast(internalContext.ConstWorldPosition, -internalContext.ConstTransform.forward, out hit, 1f)) 
		{
			IsActive = true;
		}
		else if(collidesRight && Physics.Raycast(internalContext.ConstWorldPosition, internalContext.ConstTransform.forward, out hit, 1f)) 
		{
			IsActive = true;
		}

		if (IsActive)
		{
			normals += hit.normal;
			normals *= hit.distance * 10;
			internalContext.LocalAngularDirection += internalContext.ConstTransform.InverseTransformDirection(normals);
		}
	}
	
	#endregion

	void OnDrawGizmos ()
	{
		if(IsActive)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawRay(this.transform.position, normals);
		}
	}
}
