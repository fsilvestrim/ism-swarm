﻿using UnityEngine;
using System.Collections;

public class FishAgent : AbstractAgent<FishContext, AquariumAgentContext> 
{
	private const float ROTATION_EPYSLON = 0.05f;
	Vector3 WorldDirection;

	override protected void Start()
	{
		base.Start();
		//Time.timeScale = 0.1f;
		//init internal context
		UpdateConsts();
		internalContext.LocalAngularDirection = Vector3.zero;
		internalContext.Speed = 0;
	}

	private void UpdateConsts()
	{
		internalContext.ConstTransform = transform;
		internalContext.ConstWorldForward = -transform.right;
		internalContext.ConstWorldPosition = transform.position;
	}

	override protected void FixedUpdate()
	{
		//parent
		base.FixedUpdate();

		//consts
		UpdateConsts();

		//locals
		WorldDirection = transform.TransformDirection(internalContext.LocalAngularDirection);


		//Debug.Log(internalContext.LocalAngularDirection);

		// singal actuators
		if(internalContext.Speed > 0)
		{
			var forwardDot = Vector3.Dot(WorldDirection, internalContext.ConstWorldForward);
			CallActuator<ActuatorFin>("fin_back", internalContext.Speed >= 0, Mathf.Max(0.3f * internalContext.Speed, forwardDot));

			var rotDot = Vector3.Dot(WorldDirection, transform.forward);

			//var pAngle = 180/Mathf.DeltaAngle(360, Vector3.Angle(WorldDirection, internalContext.Forward));

			CallActuator<ActuatorFin>("fin_left", rotDot < 0, rotDot);
			CallActuator<ActuatorFin>("fin_right", rotDot > 0, rotDot);
		}

		//update internals

		internalContext.LocalAngularDirection = Vector3.zero;
	}

	void OnDrawGizmos ()
	{
		Gizmos.color = Color.cyan;
		Gizmos.DrawRay(this.transform.position, WorldDirection);
	}
}